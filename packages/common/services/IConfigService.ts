import CacheServerDto from './dto/CacheServerDto';

export default interface IConfigService {
    getStage(): string;
    getProjectName(): string;
    getModule(): string;
    getApp(): string;
    getBranch(): string;
    getPipeLineRegion(): string;
    getPipeLineAccountId(): string;
    getPipeLineBranchShortName(): string;
    getPipeLineProxy(): string;
    getPipeLineNoProxy(): string;
    getBuild(tableName?: string): string;
    getPortfolio(): string;
    getComponent(): string;
    cache(): CacheServerDto;
    getValue(key: string): any;
    getQueueEndpoint(): string;
}
