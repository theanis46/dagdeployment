export default interface IStringService {
    replaceWithObjectProp(templateString: string, data: any): string;
    getSubstring(sourceString: string, startPosition: number, length: number): string;
    hasNoSpaceInBetween(input: string): boolean;
    trimLeft(input: string, charList: string); //charList is comma separated characters
}