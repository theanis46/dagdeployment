import ServiceException from './ServiceException';

export default class ResourceNotFoundException extends ServiceException {
    constructor(message?: string) {
        super(message);
    }
}