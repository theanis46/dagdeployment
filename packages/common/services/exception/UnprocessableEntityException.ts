import ServiceException from './ServiceException';

export default class UnprocessableEntityException extends ServiceException {
    constructor(message?: string, error?: any, body?: string)  {
        super(message);
        this.error = error;
        this.body = body;
    }
    error: any;
    body: string;
}
