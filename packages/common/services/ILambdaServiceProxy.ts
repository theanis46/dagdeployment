import ServiceName from '../enum/ServiceName';
import ServiceAddress from '../enum/ServiceAddress';

export interface LambdaProxyRequest {
    serviceName?: ServiceName;
    address: ServiceAddress;
    token?: string;
    payload?: any;
    formData?: any;
}

export default interface ILambdaServiceProxy {
    get(request: LambdaProxyRequest): Promise<any>;
    post(request: LambdaProxyRequest): Promise<any>;
}