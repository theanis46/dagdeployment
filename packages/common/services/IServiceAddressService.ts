import ServiceName from '../enum/ServiceName';

export default interface IServiceAddressService {
    getHost(path: string, serviceName: ServiceName): string;
}