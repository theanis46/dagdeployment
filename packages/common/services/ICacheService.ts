export type cacheHandler = () => Promise<any>;
export type cacheHitHandler = (data: any) => Promise<any>;

export default interface ICacheService {
    set(key: string, value: any, ttlInSeconds?: number): Promise<any>;
    get(key: string): Promise<any>;
    getData(key: string, handler: cacheHandler, ttlInSeconds?: number, cacheHitHandler?: cacheHitHandler): Promise<any>;
}