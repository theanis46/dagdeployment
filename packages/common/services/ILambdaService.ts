import LambdaResponseDto from './dto/LambdaResponseDto';

export interface LambdaRequest {
    address: string;
    token: string;
    payload?: any;
    formData?: any;
}

export default interface ILambdaService {
    invoke(funcName: string, payload: any): Promise<LambdaResponseDto>;
    httpPost(request: LambdaRequest): Promise<LambdaResponseDto>;
}