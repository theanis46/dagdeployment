import HttpMethod from '../enum/HttpMethod';

export interface IHttpServiceRequest {
    url: string;
    method?: HttpMethod;
    header?: any;
    data?: any;
    formData?: any;
}

export default interface IHttpService {
    get(request: IHttpServiceRequest): Promise<any>;
    post(request: IHttpServiceRequest): Promise<any>;
    invoke(request: IHttpServiceRequest): Promise<any>;
}