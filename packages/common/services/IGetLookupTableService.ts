export interface IGetLookupTableService {
    getTableRecords(tableName: string): Promise<[]>;
}