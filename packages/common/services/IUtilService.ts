import FieldMap from '../FieldMap';

export default interface IUtilService {
    getTablename(modelClass: any);
    setTablename(target: any, tablename?: string);
    getMapFields(modelClass: any): FieldMap[];
    getCalculatedFields(modelClass: any): FieldMap[];
    setMapField(target: any, fieldName: string, dbFieldName?: string): FieldMap;
    setComputedField(target: any, fieldName: string, func: (source: any) => any): FieldMap;
    getClassname(instance: any): string;
    toObject(value: any): any;
    createObject(klass: any): any;
    createObjectFrom(klass: any, objectSource: any): any;
    decodeBase64(value: string): string;
    toJson(value: any): any;
    isHttpAddress(address: string): boolean;
    createHash(data: any): string;
    getRandomId(): string;
    getObjectValue(obj: object, field: string);
    xmlToObject(value): any;
    interpolate(template: string, props: object): string;
    cleanOptionalFields(payload: any): any;
    getValueWithoutNullOrEmpty(input: string): string;
    getEnumByValue(value: any, enumType: any): any;
    cleanUnPrintableCharacter(value: string): string;
    clone(value: any): any;
}
