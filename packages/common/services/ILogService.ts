export enum LogType {
    info,
    error,
    warning
}

export default interface ILogService {
    traceId: string;
    log(logType: LogType, message: any): Promise<void>;
    info(message: any): Promise<void>;
    error(message: any): Promise<void>;
    warning(message: any): Promise<void>;
}
