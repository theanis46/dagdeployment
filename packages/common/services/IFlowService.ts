import { ItemHandler } from '../HandlerTypes';

export default interface IFlowService {
    each<TData>(items: TData[], handler: ItemHandler, parallelCount?: number): Promise<any>;
}