import { Container } from 'inversify';
import * as AWS from 'aws-sdk';
import * as path from 'path';
import * as fs from 'fs';
import * as async from 'async';
const request = require('request');

import types from './types';
import IConfigService from './IConfigService';
import ConfigService from './implementation/ConfigService';

import IUtilService from './IUtilService';
import UtilService from './implementation/UtilService';

import IErrorHandlerService from './IErrorHandlerService';
import ErrorHandlerService from './implementation/ErrorHandlerService';

import IHttpService from './IHttpService';
import HttpService from './implementation/HttpService';

import ILogService from './ILogService';
import LogService from './implementation/LogService';

import ICacheService from './ICacheService';
import CacheService from './implementation/CacheService';

import ILambdaService from './ILambdaService';
import LambdaService from './implementation/LambdaService';

import IStringService from './IStringService';
import StringService from './implementation/StringService';

import IServiceAddressService from './IServiceAddressService';
import ServiceAddressService from './implementation/ServiceAddressService';

import ILambdaServiceProxy from './ILambdaServiceProxy';
import LambdaServiceProxy from './implementation/LambdaServiceProxy';


export function configureCommonServices(container: Container, basePath: string, config?: object): Container {
    // let traceId;
    console.log("dependencies commong")
    container.bind<AWS.Lambda>(types.LambdaClient).toConstantValue(new AWS.Lambda());
    container.bind<any>(types.Logger).toConstantValue(console);
    container.bind<any>(types.FileReader).toConstantValue(fs);
    container.bind<any>(types.AsyncLib).toConstantValue(async);
    // container.bind<string>(types.traceId).to(traceId);
    container.bind<any>(types.RequestLib).toConstantValue(request);
    container.bind<IUtilService>(types.IUtilService).to(UtilService).inSingletonScope();
    container.bind<IStringService>(types.IStringService).to(StringService).inSingletonScope();
    container.bind<IServiceAddressService>(types.IServiceAddressService).to(ServiceAddressService).inSingletonScope();
    container.bind<ILambdaServiceProxy>(types.ILambdaServiceProxy).to(LambdaServiceProxy).inSingletonScope();
 
    //environment
    config = config || _getEnvironmentVariables(basePath, container);
    container.bind<any>(types.EnvironmentVariables).toConstantValue(config);
    container.bind<any>(types.EnvironmentVariablesRef).toConstantValue(process.env);
    container.bind<IConfigService>(types.IConfigService).to(ConfigService).inSingletonScope();

    container.bind<IErrorHandlerService>(types.IErrorHandlerService).to(ErrorHandlerService).inSingletonScope();
    container.bind<IHttpService>(types.IHttpService).to(HttpService).inSingletonScope();
    container.bind<ILogService>(types.ILogService).to(LogService).inRequestScope();
    container.bind<ICacheService>(types.ICacheService).to(CacheService).inSingletonScope();
    container.bind<ILambdaService>(types.ILambdaService).to(LambdaService).inSingletonScope();
    

    const configService = container.get<IConfigService>(types.IConfigService);
    const branch = configService.getBranch() || 'local';
    if (branch === 'local') {
        AWS.config.update({ region: 'ap-southeast-1' });
    }
    container.bind<AWS.SQS>(types.QueueClient).toConstantValue(new AWS.SQS({ apiVersion: '2012-11-05' }));
    return container;
}

function _getEnvironmentVariables(basePath: string, container: Container) {
    let configs: any = {};
    configs = Object.assign(configs, process.env);

    const files = [
        path.join(basePath, 'config', 'default.json'),
        path.join(basePath, 'config', `${process.env.PIPELINE_BRANCH ? process.env.PIPELINE_BRANCH.toLowerCase() : (process.env.NODE_ENV || 'local')}.json`)
    ];
    const utilService = container.get<IUtilService>(types.IUtilService);
    files.forEach(file => {
        if (fs.existsSync(file)) {
            const content = utilService.toObject(fs.readFileSync(file).toString());
            configs = Object.assign(configs, content);
        }
    });
    return configs;
}
