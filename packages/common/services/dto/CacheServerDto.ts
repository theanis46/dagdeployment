export default class CacheServerDto {
    host: string;
    port: number;
    db: string;
    defaultTTL: number;
}