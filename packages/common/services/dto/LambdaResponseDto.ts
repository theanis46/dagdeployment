
export default class LambdaResponseDto {
    status: number;
    headers: any;
    body: any;
}