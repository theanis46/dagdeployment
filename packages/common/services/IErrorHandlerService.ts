export default interface IErrorHandlerService {
    handle(error: any, req: any, res: any, next: any);
    retry(functionToExecute: () => Promise<any>, errorClassToHandle?: any, throwError?: boolean): Promise<any>;
}
