import { injectable, inject } from 'inversify';
import setCookie = require('set-cookie-parser');
import IHttpService, { IHttpServiceRequest } from '../IHttpService';
import ILogService from '../ILogService';
import RpcServiceException from '../exception/RpcServiceException';
import AccessSecurityServiceException from '../exception/AccessSecurityServiceException';
import ServerOveraCapacityException from '../exception/ServerOveraCapacityException';
import ResourceNotFoundException from '../exception/ResourceNotFoundException';
import UnprocessableEntityException from '../exception/UnprocessableEntityException';
import HttpMethod from '../../enum/HttpMethod';
import commonServiceTypes from '../types';

@injectable()
export default class HttpService implements IHttpService {

    @inject(commonServiceTypes.RequestLib)
    private readonly _requestLib: any;

    @inject(commonServiceTypes.ILogService)
    private readonly _logService: ILogService;

    get(httpRequest: IHttpServiceRequest): Promise<any> {
        httpRequest.method = HttpMethod.get;
        return this.invoke(httpRequest);
    }

    post(httpRequest: IHttpServiceRequest): Promise<any> {
        httpRequest.method = HttpMethod.post;
        return this.invoke(httpRequest);
    }

    invoke(httpRequest: IHttpServiceRequest): Promise<any> {
        const self = this;
        return new Promise((resolve, reject) => {           
            let requestPayload = {
                url: httpRequest.url,
                headers: undefined,
                method: httpRequest.method || HttpMethod.post,
                json: false,
                form: undefined,
                body: undefined,
                qs: undefined
            };
            if (httpRequest.formData) {
                requestPayload.json = false;
                requestPayload.form = httpRequest.formData;
                requestPayload.headers = Object.assign({'Content-Type': 'application/x-www-form-urlencoded'}, httpRequest.header);
            } else {
                if (httpRequest.method === HttpMethod.post) {
                    requestPayload.json = true;
                    requestPayload.body = httpRequest.data || {};
                } else {
                    requestPayload.qs = httpRequest.data || {};
                }
                requestPayload.headers = httpRequest.header;
            }
            self._logService.info({
                seviceName: 'HttpService',
                requestPayload: requestPayload
            });
            self._requestLib(requestPayload, function (error, response, body){
                self._logService.info({
                    seviceName: 'HttpService',
                    statusCode: response ? response.statusCode: 'empty response',
                    body: body
                });
                if (error) {
                    self._logService.error({
                        seviceName: 'HttpService',
                        error: error
                    });
                    reject(error);
                } else {
                    let status = parseInt(response.statusCode);
                    if (status >= 200 && status < 300) {
                        resolve({
                            status: status,
                            headers: response.headers,
                            cookies: self._decodeCookie(response),
                            response: body
                        });
                    } else if (status === 401) {
                        reject(new AccessSecurityServiceException(body));
                    } else if (status === 404) {
                        reject(new ResourceNotFoundException(body));
                    } else if (status === 503) {
                        reject(new ServerOveraCapacityException('Server over capacity.'));
                    } else if (status === 422) {
                        reject(new UnprocessableEntityException('Unprocessable Entity', error, body));
                    } else {
                        reject(new RpcServiceException(body || 'HttpService Server error'));
                    }
                }
            });
        });
    }

    private _decodeCookie(response: any): any {
        let cookies = undefined;
        if (response && response.headers) {
            cookies = setCookie.parse(response, {
                decodeValues: true
            });
        }
        return cookies;
    }
}