import { injectable, inject } from 'inversify';
import * as _ from 'underscore';
import * as AWS from 'aws-sdk';
import ILambdaService, { LambdaRequest } from '../ILambdaService';
import commonServiceTypes from '../types';
import LambdaResponseDto from '../dto/LambdaResponseDto';
import BaseService from './BaseService';
import RpcServiceException from '../exception/RpcServiceException';

const toUrlEncoded = obj => Object.keys(obj).map(k => encodeURIComponent(k) + '=' + encodeURIComponent(obj[k])).join('&');

@injectable()
export default class LambdaService extends BaseService<LambdaResponseDto> implements ILambdaService {

    @inject(commonServiceTypes.LambdaClient)
    private _lambdaClient: AWS.Lambda;

    getDtoClass(): any {
        return LambdaResponseDto;
    }

    invoke(funcName: string, payload: any): Promise<LambdaResponseDto> {
        console.log('------ invoking with payload', funcName, payload)
        let self = this;
        return new Promise((resolve, reject) => {
            console.log('------ _lambdaClient request 11:32am');
            let request = {
                FunctionName: funcName,
                Payload: this._utilService.toJson(payload)
            };
            console.log('------ request --', request);
            console.log('------ _lambdaClient invoke', this._lambdaClient.invoke)
            this._lambdaClient.invoke(request, function (err, response) {
                if (err) {
                    console.log('------------ INVOKE ERROR ', err);
                    reject(err);
                } else {
                    console.log('------------ INVOKE SUCCESS response', response);
                    let json = self._utilService.toObject(response);
                    let lambdaResponse = self._utilService.toObject(json.Payload);      
                    console.log('------------ INVOKE SUCCESS lambdaResponse', lambdaResponse);
                    if (lambdaResponse.statusCode >= 500) {
                        console.log('------------ INVOKE 500 err lambdaResponse');   
                        self._logService.info({
                            lambda: funcName,
                            response: lambdaResponse
                        });
                        reject(new RpcServiceException(lambdaResponse.body || 'lambda service error'));
                    }
                    let dto = self.toDto(lambdaResponse);
                    if (_.isString(dto.body)) {
                        dto.body = self._utilService.toObject(dto.body);
                    }

                    console.log('------------ INVOKE SUCCESS dto', dto);
                    resolve(dto);
                }
            });
        });
    }

    httpPost(request: LambdaRequest): Promise<LambdaResponseDto> {
        let address = request.address;
        let payload = request.payload;
        let token = request.token;
        let index = address.indexOf('/');
        let path = address.substring(index);
        let lambdaArn = address.substring(0, index);
        let headers = {
            'Content-Type': 'application/json',
            Authorization: 'Bearer ' + token
        };
        let encodedPostData = {};
        if (request.formData) {
            encodedPostData = toUrlEncoded(request.formData);
            headers['Content-Type'] = 'application/x-www-form-urlencoded';
        } else {
            encodedPostData = this._utilService.toJson(payload);
        }
        let lambdaPayload = {
            isBase64Encoded: false,
            body: encodedPostData,
            path: path,
            httpMethod: 'POST',
            headers: headers
        };
        return this.invoke(lambdaArn, lambdaPayload);
    }
}