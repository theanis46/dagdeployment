import { injectable} from 'inversify';
import * as objectPath from 'object-path';
import * as _ from 'underscore';
import crypto = require('crypto');
import IUtilService from '../IUtilService';
import FieldMap from '../../FieldMap';
import uuidv1 = require('uuid/v1');
import * as xmlConvert from 'xml-js';

@injectable()
export default class UtilService implements IUtilService {

    private _decodedValue: any = {};

    getTablename(modelClass: any): string {
        let tableName: string;
        if (modelClass && modelClass.$__tableName !== undefined) {
            tableName = modelClass.$__tableName;
        }
        return tableName;
    }

    setTablename(target: any, tablename?: string) {
        if (target) {
            if (!tablename) {
                tablename = this.getClassname(target);
                tablename = tablename.replace('Model', '').toLowerCase();
            }
            target.$__tableName = tablename;
        }
    }

    getMapFields(modelClass: any): FieldMap[] {
        let fields: any[] = [];
        if (modelClass && modelClass.$__mapFields !== undefined && modelClass.$__mapFields.length > 0) {
            fields = modelClass.$__mapFields.map(field => {
                return Object.assign({}, field);
            });
        }
        return fields;
    }

    getCalculatedFields(modelClass: any): FieldMap[] {
        let fields: any[] = [];
        if (modelClass && modelClass.$__computedFields !== undefined && modelClass.$__computedFields.length > 0) {
            fields = modelClass.$__computedFields.map(field => {
                return Object.assign({}, field);
            });
        }
        return fields;
    }

    setMapField(target: any, fieldName: string, dbFieldName?: string, defaultValue?: any): FieldMap {
        if (target.$__mapFields === undefined) {
            target.$__mapFields = [];
        }
        dbFieldName = dbFieldName || fieldName;
        const fieldMap = new FieldMap();
        fieldMap.destinationField = fieldName;
        fieldMap.sourceField = dbFieldName;
        fieldMap.defaultValue = defaultValue;
        target.$__mapFields.push(fieldMap);
        return fieldMap;
    }

    setComputedField(target: any, fieldName: string, func: (source: any) => any): FieldMap {
        if (target.$__computedFields === undefined) {
            target.$__computedFields = [];
        }
        const fieldMap = new FieldMap();
        fieldMap.destinationField = fieldName;
        fieldMap.defaultValue = func;
        target.$__computedFields.push(fieldMap);
        return fieldMap;
    }

    getClassname(instance: any): string {
        let clsssName: string = '';
        if (instance) {
            const typeName = typeof instance;
            if (typeName === 'object') {
                clsssName = instance.constructor.name;
            } else if (typeName === 'function') {
                clsssName = instance.name;
            } else {
                clsssName = typeName;
            }
        }
        return clsssName;
    }

    toObject(value: any, defaultValue?: any): any {
        let converted: any = value;
        if (_.isString(value) && value.length > 0) {
            try {
                converted = JSON.parse(value);
            } catch (err) {
                converted = defaultValue || {};
            }
        }
        return converted;
    }

    createObject(klass: any): any {
        // todo: hookup an afterCreateHandler for mapping
        const object = new klass();
        return object;
    }

    createObjectFrom(klass: any, objectSource: any): any {
        let model: any;
        if (klass && objectSource) {
            model = this.createObject(klass);

            // populate base mapFields
            const fields = this.getMapFields(klass);
            if (fields && fields.length > 0) {
                fields.forEach(field => {
                    const fieldName = field.destinationField;
                    const dbFieldName = field.sourceField;
                    let value = this.getObjectValue(objectSource, dbFieldName);
                    if (value === undefined && field.defaultValue !== undefined) {
                        value = field.defaultValue;
                    }
                    if (value !== undefined) {
                        model[fieldName] = value;
                    }
                });
            }

            // populate base calculated fields
            // todo: in future if calculated failed, fallback to defaultValue
            const calculatedFields = this.getCalculatedFields(klass);
            if (calculatedFields && calculatedFields.length > 0) {
                calculatedFields.forEach(field => {
                    const fieldName = field.destinationField;
                    const func = field.defaultValue;
                    if (_.isFunction(func)) {
                        const value = func(objectSource);
                        if (value !== undefined) {
                            model[fieldName] = value;
                        }
                    }
                });
            }
        }
        return model;
    }

    decodeBase64(value: string): string {
        let decodedValue: string;
        if (value) {
            decodedValue = this._decodedValue[value];
            if (!decodedValue) {
                const buffer = Buffer.from(value, 'base64');
                decodedValue = buffer.toString('ascii');
                this._decodedValue[value] = decodedValue;
            }
        }
        return decodedValue;
    }

    toJson(value: any): any {
        let converted: any = value;
        if (value && typeof value === 'object') {
            converted = JSON.stringify(value);
        }
        return converted;
    }

    isHttpAddress(address: string): boolean {
        let isHttp = false;
        if (address && address.length > 0) {
            isHttp = address.indexOf('http://') === 0 || address.indexOf('https://') === 0;
        }
        return isHttp;
    }

    createHash(data: any): string {
        const json = this.toJson(data);
        return crypto.createHash('md5').update(json).digest('hex');
    }

    getRandomId(): string {
        return uuidv1();
    }

    interpolate(template: string, props: object): string {
        let result = template;
        if (template && props) {
            const keys = Object.keys(props);
            if (keys.length > 0) {
                keys.forEach(key => {
                    const keyName = '{' + key + '}';
                    const keyValue = props[key];
                    while (result.indexOf(keyName) >= 0) {
                        result = result.replace(keyName, keyValue);
                    }
                });
            }
        }
        return result;
    }

    getObjectValue(obj: object, field: string) {
        return objectPath.get(obj, field);
    }

    xmlToObject(value): any {
        let converted = value;
        if (converted && _.isString(value)) {
            try {
                // converted = xml2js.parseString(value);
                converted = xmlConvert.xml2json(value, { compact: true, spaces: 4 });
                return this.toObject(converted);
            } catch (err) {
                converted = {};
            }
        }
        return converted;
    }

    cleanOptionalFields(payload: any): any {
        if (_.isString(payload) && payload.length > 0) {
            return payload;
        } else if (_.isNumber(payload)) {
            return payload;
        } else if (_.isBoolean(payload)) {
            return payload;
        } else if (_.isDate(payload)) {
            return payload;
        } else if (_.isObject(payload)) {
            let entries;
            const keys = Object.keys(payload);
            if (keys.length > 0) {
                entries = {};
                keys.forEach(key => {
                    const value = this.cleanOptionalFields(payload[key]);
                    if (!_.isNull(value) && !_.isUndefined(value)) {
                        entries[key] = value;
                    }
                });
                if (Object.keys(entries).length === 0) {
                    entries = undefined;
                }
            }
            return entries;
        } else if (_.isArray(payload)) {
            const entries = payload.filter(entry => {
                const value = this.cleanOptionalFields(entry);
                return _.isNull(value) === false && _.isUndefined(value) === false;
            });
            if (entries.length > 0) {
                return entries;
            }
        }
    }

    getValueWithoutNullOrEmpty(input: string): string {
        return input && input.trim().length > 0 ? input : ' ';
    }

    getEnumByValue(value: any, enumType: any): any {
        const values = Object.values(enumType);
        const keys = Object.keys(enumType);
        return keys[values.indexOf(value)];
    }

    cleanUnPrintableCharacter(value: string): string {
        // description: this function is only a hotfix until the unknown character from the app is discover
        let cleanValue;
        if (value) {
            const parts = value.trim().split(' ');
            cleanValue = parts.filter(x => x.trim().length > 0).join(' ');
        }
        return cleanValue;
    }

    clone(value: any): any {
        return JSON.parse(JSON.stringify(value));
    }
}
