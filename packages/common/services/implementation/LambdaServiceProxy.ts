import { injectable, inject } from 'inversify';
import commonServiceTypes from '../types';
import IHttpService from '../IHttpService';
import ILogService from '../ILogService';
import IUtilService from '../IUtilService';
import IServiceAddressService from '../IServiceAddressService';
import ILambdaServiceProxy, { LambdaProxyRequest } from '../ILambdaServiceProxy';

@injectable()
export default class LambdaServiceProxy implements ILambdaServiceProxy {

    @inject(commonServiceTypes.IHttpService)
    private _httpService: IHttpService;

    @inject(commonServiceTypes.IServiceAddressService)
    private _serviceAddressService: IServiceAddressService;

    @inject(commonServiceTypes.ILogService)
    private _logService: ILogService;

    @inject(commonServiceTypes.IUtilService)
    private _utilService: IUtilService;

    get(request: LambdaProxyRequest): Promise<any> {
        const httpPayload = this._createHttpRequest(request);
        this._logService.info({
            host: httpPayload.url
        });
        return this._httpService
            .get(httpPayload)
            .then(response => {
                const result = this._decodeResponse(response);
                this._logService.info({
                    seviceName: "LambdaServiceProxy",
                    host: httpPayload.url,
                    response: result
                });
                return result;
            });
    }

    post(request: LambdaProxyRequest): Promise<any> {
        const httpPayload = this._createHttpRequest(request);
        this._logService.info({
            host: httpPayload.url
        });
        return this._httpService
            .post(httpPayload)
            .then(response => {
                const result = this._decodeResponse(response);
                this._logService.info({
                    seviceName: "LambdaServiceProxy",
                    host: httpPayload.url,
                    response: result
                });
                return result;
            });
    }

    private _createHttpRequest(request: LambdaProxyRequest) {
        let address = request.address;
        let host = this._serviceAddressService.getHost(address, request.serviceName);
        let token = request.token;
        let headers: any = {
            'Content-Type': 'application/json'
        };
        if (token) {
            headers['Authorization'] = 'Bearer ' + token;
        }
        return {
            url: host,
            header: headers,
            data: request.payload
        };
    }

    private _decodeResponse(response): any {
        return this._utilService.toObject(response.response);
    }
}