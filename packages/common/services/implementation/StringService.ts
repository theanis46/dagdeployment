import { injectable } from 'inversify';
import IStringService from '../IStringService';

@injectable()
export default class StringService implements IStringService {

    replaceWithObjectProp(templateString: string, data: any): string {
        return templateString.replace(
            /{(\w*)}/g,
            function (match, key) {
                return data.hasOwnProperty(key) ? data[key] : `${match}`;
            }
        );
    }

    // The position value starts from 1 so we need to convert it to start from 0 here
    getSubstring(sourceString: string, position: number, length: number): string {
        return sourceString.length > position + length - 2 ? sourceString.substring(position - 1, position - 1 + length) : "";
    }

    hasNoSpaceInBetween(input: string): boolean {
        const chars = input.trim().split(" ");
        return chars.length == 1;
    }

    trimLeft(input: string, charList: string) {
        if(input){
            if (charList === undefined)
            charList = "\s";

            return input.replace(new RegExp("^[" + charList + "]+"), "");
        }
    }
}