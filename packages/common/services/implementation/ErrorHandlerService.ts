import { injectable, inject } from 'inversify';
import IErrorHandlerService from '../IErrorHandlerService';
import ILogService from '../ILogService';
import commonServiceTypes from '../types';
import AccessSecurityServiceException from '../exception/AccessSecurityServiceException';
import RpcServiceException from '../exception/RpcServiceException';
import ResourceNotFoundException from '../exception/ResourceNotFoundException';
import RepositoryException from '../../repositories/exception/RepositoryException';
import DbSaveException from '../../repositories/exception/DbSaveException';

@injectable()
export default class ErrorHandlerService implements IErrorHandlerService {

    @inject(commonServiceTypes.ILogService)
    private _logService: ILogService;

    // @ts-ignore
    handle(error: any, req: any, res: any, next: any) {
        let status: string = '500';
        if (error instanceof AccessSecurityServiceException || error.innerException instanceof AccessSecurityServiceException) {
            status = '401';
        } else if (error instanceof ResourceNotFoundException) {
            status = '404';
        } else if (error instanceof RpcServiceException || error.innerException instanceof RpcServiceException) {
            status = '500';
        } else {
            status = error.status;
        }
        let payload = {
            message: error.message,
            error: error
        };
        if (!status) {
            status = '500'
        }
        let statusId = parseInt(status);
        if (statusId === 401) {
            payload.message = 'access denied';
        }
        this._logService.error(error);
        res.status(statusId);
        res.send(payload);
    }

    async retry(functionToExecute: () => Promise<any>, errorClassToHandle?: any, throwError?: boolean): Promise<any> {
        const delayTime = 500;
        let timesToRetry = 5;
        let result;
        if (throwError === undefined) {
            throwError = false;
        }
        while (true) {
            if (timesToRetry <= 0) {
                if (throwError === true) {
                    throw new DbSaveException('Maximum retry reached.');
                } else {
                    //description: break into the loop as per original behavior and success is assume
                    break;
                }
            }
            try {
                result = await functionToExecute();
                break;
            } catch (error) {
                if (error instanceof errorClassToHandle) {
                    await this.timeout(delayTime); // sleep for 500ms
                    timesToRetry--;
                } else {
                    this._logService.error({
                        type: 'Retry failed',
                        error
                    });
                    result = Promise.reject(new RepositoryException('Retry failed'));
                    return result;
                }
            }
        }
        return result;
    }

    private timeout(ms: number): Promise<any> {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}
