import { injectable, inject } from 'inversify';
import ILogService, { LogType } from '../ILogService';
import IUtilService from '../IUtilService';
import commonServiceTypes from '../types';

@injectable()
export default class LogService implements ILogService {
    traceId: string;

    constructor(){
        console.log("Constructor called " + this.traceId);
    }

    @inject(commonServiceTypes.IUtilService)
    private _utilService: IUtilService;

    @inject(commonServiceTypes.Logger)
    private _logger: any;

    // @inject(commonServiceTypes.traceId)
    // private _traceid: string;

    log(logType: LogType, message: any): Promise<void> {
        //console.log(this._traceid);

        if (logType === LogType.error) {
            return this.error(message);
        } else if (logType === LogType.warning) {
            return this.warning(message);
        } else {
            return this.info(message);
        }
    }

    info(message: any): Promise<void> {
        let payload = this._utilService.toJson(message);
        this._logger.info(payload);
        return Promise.resolve();
    }

    error(message: any): Promise<void> {
        let payload = this._utilService.toJson(message);
        this._logger.error(payload);
        return Promise.resolve();
    }

    warning(message: any): Promise<void> {
        let payload = this._utilService.toJson(message);
        this._logger.warn(payload);
        return Promise.resolve();
    }
}