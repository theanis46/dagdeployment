import { injectable, inject } from 'inversify';
import * as objectPath from 'object-path';
import IConfigService from '../IConfigService';
import IUtilService from '../IUtilService';
import commonServiceTypes from '../types';
import CacheServerDto from '../dto/CacheServerDto';

@injectable()
export default class ConfigService implements IConfigService {

    private readonly _cacheValue = {};

    @inject(commonServiceTypes.EnvironmentVariables)
    protected _environmentVariables: any;

    @inject(commonServiceTypes.IUtilService)
    protected _utilService: IUtilService;

    getStage(): string {
        return this._environmentVariables.projectStage || 'dev';
    }

    getProjectName(): string {
        return this._environmentVariables.projectName || 'default';
    }

    getModule(): string {
        return this._environmentVariables.module || 'default';
    }

    getApp(): string {
        return this._environmentVariables.PIPELINE_APP;
    }

    getBranch(): string {
        return this._environmentVariables.PIPELINE_BRANCH;
    }

    getPipeLineRegion(): string {
        return this._environmentVariables.PIPELINE_AWS_REGION;
    }

    getPipeLineAccountId(): string {
        return this._environmentVariables.PIPELINE_AWS_ACCOUNT_ID;
    }

    getPipeLineBranchShortName(): string {
        return this._environmentVariables.PIPELINE_BRANCH_SHORT_NAME;
    }

    getPipeLineProxy(): string {
        return this._environmentVariables.PIPELINE_PROXY_URL;
    }

    getPipeLineNoProxy(): string {
        return this._environmentVariables.PIPELINE_NO_PROXY;
    }

    getBuild(tableName?: string): string {
        if (tableName && this._environmentVariables.build && this._environmentVariables.build[tableName]) {
            return this._environmentVariables.build[tableName];
        }

        return this._environmentVariables.PIPELINE_BUILD;
    }

    getPortfolio(): string {
        return this._environmentVariables.PIPELINE_PORTFOLIO;
    }

    getComponent(): string {
        return this._environmentVariables.PIPELINE_COMPONENT;
    }
  
    cache(): CacheServerDto {
        return {
            host: this._environmentVariables.redis_host || 'localhost',
            port: parseInt(this._environmentVariables.redis_port || '6379'),
            db: this._environmentVariables.redis_db || '1',
            defaultTTL: parseInt(this._environmentVariables.defaultCacheInSeconds || '3600')
        };
    }

    getValue(key: string): any {
        if (this._cacheValue[key] === undefined) {
            this._cacheValue[key] = objectPath.get(this._environmentVariables, key);
        }
        return this._cacheValue[key];
    }

    getQueueEndpoint(): string {
        return this.getValue('queue.host');
    }
}
