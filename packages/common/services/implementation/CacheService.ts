import { injectable, inject } from 'inversify';
const redis = require('redis');
import ICacheService, { cacheHandler, cacheHitHandler } from '../ICacheService';
import IConfigService from '../IConfigService';
import IUtilService from '../IUtilService';
import commonServiceTypes from '../types';

@injectable()
export default class CacheService implements ICacheService {    

    @inject(commonServiceTypes.IConfigService)
    private _configService: IConfigService;

    @inject(commonServiceTypes.IUtilService)
    private _utilService: IUtilService;

    private _client: any;

    async set(key: string, value: any, ttlInSeconds?: number): Promise<any> {
        let cacheKey = this._createKey(key);
        await this._connect();
        const cacheConfig = this._configService.cache();
        ttlInSeconds = ttlInSeconds || cacheConfig.defaultTTL;
        return new Promise((resolve, reject) => {
            if (value === undefined || value === null) {
                // @ts-ignore
                this._client.del(cacheKey, (err, response) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(value);
                    }
                });
            } else {
                this._client.set(cacheKey, this._utilService.toJson(value), (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        this._client.expire(cacheKey, ttlInSeconds);
                        resolve(value);
                    }
                });
            }
        });       
    }

    async get(key: string): Promise<any> {       
        await this._connect();
        return new Promise((resolve, reject) => {
            let cacheKey = this._createKey(key);
            this._client.get(cacheKey, (err, value) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(this._utilService.toObject(value));
                }
            });
        });
    }

    async getData(key: string, handler: cacheHandler, ttlInSeconds?: number, cacheHitHandler?: cacheHitHandler): Promise<any> {
        let cacheData = await this.get(key);
        if (cacheData) {
            cacheData = this._utilService.toObject(cacheData);
            if (cacheHitHandler) {
                cacheData = await cacheHitHandler(cacheData);
            }
        } else {
            let data = await handler();           
            this.set(key, data, ttlInSeconds);
            cacheData = data;
        }
        return cacheData;
    }

    private _createKey(key: string) {
        return [this._configService.getStage(), this._configService.getModule(), key].join(':');
    }

    private _connect(): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this._client == null) {
                const cacheConfig = this._configService.cache();
                let redisHost = cacheConfig.host;
                let redisPort = cacheConfig.port;
                let redisDb = cacheConfig.db;
                this._client  = redis.createClient({
                    host: redisHost,
                    redisPort: redisPort,
                    db: redisDb
                });
                this._client .on('connect', () => {
                    resolve(this._client );
                });
                this._client .on('error', (err) => {
                    reject(err);
                });
            } else {
                resolve(this._client);
            }
        });
    }
}