import { injectable, inject } from 'inversify';
import commonServiceTypes from '../types';
import IServiceAddressService from '../IServiceAddressService';
import IConfigService from '../IConfigService';
import IUtilService from '../IUtilService';
import ServiceName from '../../enum/ServiceName';

@injectable()
export default class ServiceAddressService implements IServiceAddressService {

    @inject(commonServiceTypes.IConfigService)
    private _configService: IConfigService;

    @inject(commonServiceTypes.IUtilService)
    private _utilService: IUtilService;

    getHost(path: string, serviceName?: ServiceName): string {
        let host: string = '';
        const parameters = {
            branchName: this._configService.getPipeLineBranchShortName(),
            build: this._configService.getBuild(),
            appName: this._configService.getApp()
        };
        if(serviceName) {
            // multiple alb logic
            const serviceUrl = this._configService.getValue('alb.byService.' + serviceName);
            host = serviceUrl || this._configService.getValue('alb.default') 
        } else {
            // default single alb logic
            host = this._configService.getValue('alb.default');
        }
        host = this._utilService.interpolate(host, parameters);        
        return host + path;
    }
}