export default function () {
    // @ts-ignore
    return function (req, res, next) {
        console.log("cors middleware");
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Origin");
        next();
    }
}