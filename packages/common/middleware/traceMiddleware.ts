import uuidv1 = require('uuid/v1');
export default () => {
    // @ts-ignore
    return (req, res, next) => {
        if (req.headers.correlationid) {
            console.log('CorrelationId Exists' + req.headers.correlationid);
        } else {
            req.headers.correlationid = uuidv1();
            console.log('Creating New Correlationid: ' + req.headers.correlationid);
        }
        next();
    };
};
