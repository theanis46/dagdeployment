const enum StatusMessage {
    OK = 'Success',
    GenericError = 'Something went wrong.',
    BadRequest = 'Bad Request',
    NotFound = 'Not Found'
}
export default StatusMessage;
