const enum HttpStatusCode {
    OK = '200',
    BadRequest = '400',
    GenericError = '500',
    NotFound = '404'
}
export default HttpStatusCode;
