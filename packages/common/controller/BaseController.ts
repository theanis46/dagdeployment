import { inject } from 'inversify';
import { controller } from 'inversify-express-utils';
import { Request, Response } from 'express';
import IUtilService from '../services/IUtilService';
import ILogService from '../services/ILogService';
import comonServiceTypes from '../services/types';
import * as express from 'express';
import HttpStatusCode from '../enum/StatusCode';

@controller('/')
export default abstract class BaseController {

    @inject(comonServiceTypes.IUtilService)
    protected _utilService: IUtilService;

    @inject(comonServiceTypes.ILogService)
    protected _logService: ILogService;

    returnValue(handler: () => Promise<any>, response: Response): Promise<any> {
        return Promise.resolve()
            .then(handler)
            .then(result => {
                console.log("result %j",result);
                // if (result === undefined) {
                //     response.status(404);
                //     response.send({});
                // } else 
                if (result.statusCode === HttpStatusCode.BadRequest) {
                    response.status( parseInt(HttpStatusCode.BadRequest) );
                    response.send(result);
                } else {
                    response.status( parseInt(HttpStatusCode.OK) );
                    response.send(result);
                }
            });
    }

    params(request: express.Request): any {
        let parameters = {};
        if (request) {
            /********************************************\
             * description: this method will collect all the parameters for the request with the priority below
             *
             * 1. url params - all methods
             * 2. body - if method post/put
             * 3. query params -  all methods
             *
            \********************************************/

            // query params for all methods
            parameters = Object.assign(parameters, request.query || {});

            const httpMethod = request.method.toLowerCase();
            if (httpMethod === 'post' || httpMethod === 'put') {
                parameters = Object.assign(parameters, request.body || {});
            }

            // url params for all methods
            parameters = Object.assign(parameters, request.params || {});
        }
        return parameters;
    }

    sanitiseRequest(request: Request) {
        if (request.method.toLowerCase() === 'post' && request.body && Buffer.isBuffer(request.body)) {
            try {
                request.body = this._utilService.toObject(request.body.toString());
            } catch (err) {
              throw err;
            }
        }
    }
}
