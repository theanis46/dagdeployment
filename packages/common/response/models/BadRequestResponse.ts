import { BaseResponse } from './BaseResponse';
import StatusCode from '../../enum/StatusCode';
import StatusMessage from '../../enum/StatusMessage';

export class BadRequestResponse<T> extends BaseResponse<T> {
    statusCode: string;
    statusMessage: string;
    data: T;

    constructor(response: T, statusCode: string = StatusCode.BadRequest, statusMessage: string = StatusMessage.BadRequest) {
        super(response, statusCode, statusMessage);
    }
}
