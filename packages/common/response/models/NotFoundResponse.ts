import { BaseResponse } from './BaseResponse';
import HttpStatusCode from '../../enum/StatusCode';
import StatusMessage from '../../enum/StatusMessage';

export class NotFoundResponse<T> extends BaseResponse<T> {
    statusCode: string;
    statusMessage: string;
    data: T;

    constructor(response: T, statusCode: string = HttpStatusCode.NotFound, statusMessage: string = StatusMessage.NotFound) {
        super(response, statusCode, statusMessage);
    }
}
