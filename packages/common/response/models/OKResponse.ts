import { BaseResponse } from './BaseResponse';
import HttpStatusCode from '../../enum/StatusCode';
import StatusMessage from '../../enum/StatusMessage';

export class OKResponse<T> extends BaseResponse<T> {
    statusCode: string;
    statusMessage: string;
    data: T;

    constructor(response: T, statusCode: string = HttpStatusCode.OK, statusMessage: string = StatusMessage.OK) {
        super(response, statusCode, statusMessage);
    }
}
