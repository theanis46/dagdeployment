export abstract class BaseResponse<T> {
    statusCode: string;
    statusMessage: string;
    data: T;

    constructor(response: T, statusCode: string, statusMessage: string) {
        this.data = response;
        this.statusCode = statusCode;
        this.statusMessage = statusMessage;
    }
}
