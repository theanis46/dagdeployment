export interface IBaseRepository<TModel> {
    getById(id: string): Promise<TModel>;
    getByIds(ids: string[]): Promise<TModel[]>;
}