import { Container } from "inversify";
import * as AWS from 'aws-sdk';
import types from './types';
import IConfigService from '../services/IConfigService';
import commonServiceTypes from '../services/types';

export function configureCommonRepositories(container: Container): Container {
    const configService = container.get<IConfigService>(commonServiceTypes.IConfigService);
    const branch = configService.getBranch() || 'local';

    if (branch === 'local') {
        const awsConfig: any = {
            region: branch,
            endpoint: new AWS.Endpoint(configService.getValue('database.host'))
        };
        AWS.config.update(awsConfig);
        container.bind<AWS.DynamoDB.DocumentClient>(types.DynamodbClient).toConstantValue(new AWS.DynamoDB.DocumentClient());
    } else {
        container.bind<AWS.DynamoDB.DocumentClient>(types.DynamodbClient).toConstantValue(new AWS.DynamoDB.DocumentClient());
    }
    return container;
}
