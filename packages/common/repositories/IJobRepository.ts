import { JobStore } from '../enum/JobStore';

export interface IJobRepository {
    save(store: JobStore, job: any): Promise<any>;
    read(store: JobStore): Promise<any[]>;
    delete(store: JobStore, receiptId: string): Promise<void>;
}
