import RepositoryException from './RepositoryException';

export default class ConditionalCheckFailedException extends RepositoryException {
    constructor(message?: string, code?: string)  {
        super(message);
        this.code = code;
    }
    code: string;
}
