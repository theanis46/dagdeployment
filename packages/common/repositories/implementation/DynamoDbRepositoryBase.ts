import { injectable, inject } from 'inversify';
import * as AWS from 'aws-sdk';
import IUtilService from '../../services/IUtilService';
import commonServiceTypes from '../../services/types';
import commonRepositoryTypes from '../../repositories/types';
import IConfigService from '../../services/IConfigService';
import ILogService from '../../services/ILogService';
import ConditionalCheckFailedException from '../exception/ConditionalCheckFailedException';
import RepositoryException from '../exception/RepositoryException';

export interface filterCriteria {
    tableName?: string;
    indexName?: string;
    predicate?: string;
    field?: any;
    value?: any;
}

export interface updateCriteria {
    key: any;
    tableName?: string;
    conditionExpression?: string;
    updateExpression?: any;
    expressionAttributeValues?: any;
    expressionAttributeNames?: any;
}

@injectable()
export default abstract class DynamoDbRepositoryBase<TModel> {

    @inject(commonRepositoryTypes.DynamodbClient)
    private _dynamoDbClient: AWS.DynamoDB.DocumentClient;

    @inject(commonServiceTypes.IUtilService)
    protected _utilService: IUtilService;

    @inject(commonServiceTypes.IConfigService)
    protected _configService: IConfigService;

    @inject(commonServiceTypes.ILogService)
    protected _logService: ILogService;

    //note: due to type lack of runtime lost of metadata let's make this explicit
    abstract getModelClass(): Function;

    async find(criteria: filterCriteria, modelClass?: any): Promise<TModel[]> {
        let models: TModel[] = [];
        let params: any = {
            TableName: criteria.tableName || this.getTableName()
        };

        let response;
        if (criteria.indexName) {
            params.IndexName = criteria.indexName;
            params.KeyConditionExpression = criteria.predicate;
            params.ExpressionAttributeNames = criteria.field;
            params.ExpressionAttributeValues = criteria.value;

            this._logService.info({
                method: "find/query",
                params: params
            });

            response = await this._dynamoDbClient.query(params).promise();
        } else {

            this._logService.info({
                method: "find/scan",
                params: params
            });

            response = await this._dynamoDbClient.scan(params).promise();
        }

        if (response && response.Items && response.Items.length > 0) {
            models = response.Items.map(dbModel => {
                let model = this._utilService.createObjectFrom(modelClass || this.getModelClass(), dbModel);
                return model;
            });
        }
        return models;
    }

    async findOne(criteria: filterCriteria): Promise<TModel> {
        let dataModel: any = null;
        var items = await this.find(criteria);
        if (items && items.length > 0) {
            dataModel = items[0];
        }
        return dataModel;
    }

    async getById(id: string, fieldId: string = 'id'): Promise<TModel> {
        let model: TModel;
        let criteria = {};
        criteria[fieldId] = id;
        var params = {
            TableName: this.getTableName(),
            Key: criteria
        };
        var dbModel = await this._dynamoDbClient.get(params).promise();
        if (dbModel && dbModel.Item) {
            model = this._utilService.createObjectFrom(this.getModelClass(), dbModel.Item);
        }
        return model;
    }

    async getByIds(ids: string[], fieldId: string = 'id'): Promise<TModel[]> {
        let models;
        const tableName = this.getTableName();
        var params = {
            RequestItems: {
                [tableName]: {
                    Keys: <any>ids.map(id => {
                        return {
                            [fieldId]: id
                        }
                    })
                }
            }
        };
        const response = await this._dynamoDbClient.batchGet(params).promise();
        if (response && response.Responses) {
            const results = response.Responses[tableName];
            const modelClass = this.getModelClass();
            models = results.map(model => {
                return this._utilService.createObjectFrom(modelClass, model);
            });
        }
        return models || [];
    }

    async putItemWithCondition(model: any, condition: string): Promise<TModel> {
        if (model) {
            const dbModel = { ...model };
            delete dbModel['$__tableName'];

            const params = {
                TableName: this.getTableName(this.getModelClass() || model),
                Item: dbModel,
                ConditionExpression: condition
            };
            try {
                await this._dynamoDbClient.put(params).promise();
            } catch(ex) {
                if (ex.code.toUpperCase() === 'ConditionalCheckFailedException'.toUpperCase()) {
                    throw new ConditionalCheckFailedException(
                        'DB insert failed.',
                        ex.code
                    );
                } else {
                    throw new RepositoryException(`DynamoDB Error: ${ex}`);
                }
            }
        }
        return model;
    }

    async PutItem(model: any): Promise<TModel> {
        if (model) {
            const dbModel = { ...model };
            delete dbModel['$__tableName'];

            const params = {
                TableName: this.getTableName(this.getModelClass() || model),
                Item: dbModel
            };

            await this._dynamoDbClient.put(params).promise();
        }
        return model;
    }

    async updateItem(criteria: updateCriteria): Promise<TModel> {
        let model;
        if (criteria.key) {
            let params: any = {
                TableName: criteria.tableName || this.getTableName(),
                Key: criteria.key,
                ReturnValues: "UPDATED_NEW"
            };

            if (criteria.conditionExpression) {
                params.ConditionExpression = criteria.conditionExpression;
            }
            if (criteria.updateExpression) {
                params.UpdateExpression = criteria.updateExpression;
            }
            if (criteria.expressionAttributeValues) {
                params.ExpressionAttributeValues = criteria.expressionAttributeValues;
            }
            if (criteria.expressionAttributeNames) {
                params.ExpressionAttributeNames = criteria.expressionAttributeNames;
            }
            try {
                model = await this._dynamoDbClient.update(params).promise();
            } catch(ex) {
                if (ex.code.toUpperCase() === 'ConditionalCheckFailedException'.toUpperCase()) {
                    throw new ConditionalCheckFailedException(
                        'DB update failed.',
                        ex.code
                    );
                } else {
                    throw new RepositoryException(`DynamoDB Error: ${ex}`);
                }
            }
        }
        return model;
    }

    async deleteItem(id: string, fieldId: string = 'id'): Promise<any> {
        let criteria = {};
        criteria[fieldId] = id;
        var params = {
            TableName: this.getTableName(),
            Key: criteria
        };
        let result = await this._dynamoDbClient.delete(params).promise();
        return result;
    }


    protected getTableName(modelClass?: any): string {
        const portfolio = this._configService.getPortfolio();
        const app = this._configService.getApp();
        const branch = this._configService.getBranch();
        
        let tableNameBaseOnMetaData = this._utilService.getTablename(modelClass || this.getModelClass());
        // sia format {{ context.Portfolio }}-{{ context.App }}-{{ context.Branch }}-{{ context.Build }}-dynamo
        
        const build = this._configService.getBuild(tableNameBaseOnMetaData);

        const parts = [portfolio, app, branch, build, tableNameBaseOnMetaData];
        const tableName = parts.filter(x => x).join('-');

        //console.log('-------- dynamoDb table name -------', tableName.toLowerCase());
        this._logService.info('-------- dynamoDb table name -------' + tableName.toLowerCase());
        return tableName.toLowerCase();
    }
}