import proxyHandler = require('serverless-http');

export default function(app) {
    return proxyHandler(app);
}