import * as express from 'express';
import { inject } from 'inversify';
import { controller, httpGet } from 'inversify-express-utils';
import serviceTypes from '../services/types';
import BaseController from '../../common/controller/BaseController';
import ISeedService from '../services/ISeedService';

@controller('/service')
export default class SeedController extends BaseController {
    @inject(serviceTypes.ISeedService)
    private _seedService: ISeedService;

    @httpGet("/Seed/details/:SeedId")
    getById(request: express.Request, response: express.Response) {
        const params = this.params(request);
        const { Id } = params;

        this._logService.info({
            name: 'SeedController',
            params: params
        });

        console.log("type "+typeof(this._logService));

        this._logService.traceId= "Trace Id";
        return this.returnValue(() => {
            return this._seedService.getById(Id);
        }, response);
    }
}
