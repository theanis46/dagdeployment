export default interface ISeedRepository {
    getById(id: string): Promise<any>;
}