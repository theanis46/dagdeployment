import { Container } from "inversify";
import ISeedRepository from "./ISeedRepository";
import SeedRepository from "./implementaion/SeedRepository";
import seedRepositoryTypes from "./types";

export function configureSeedRepositories(container: Container) {
    container.bind<ISeedRepository>(seedRepositoryTypes.ISeedRepository).to(SeedRepository).inSingletonScope();
    return container;
}