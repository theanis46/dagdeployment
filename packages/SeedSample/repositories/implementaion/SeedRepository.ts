import { injectable, inject} from "inversify";
import * as _ from 'underscore';
import ISeedRepository from "../ISeedRepository";
import LogService from '../../../common/services/implementation/LogService'
import comonServiceTypes from '../../../common/services/types';

@injectable()
export default class SeedRepository implements ISeedRepository {

    @inject(comonServiceTypes.ILogService)
    protected _logService: LogService;

    getModelClass(): Function {
        throw new Error("Method not implemented.");
    }

    protected getDtoClass(): Function {
        return null;
    }

    async getById(id: string): Promise<any> {
        console.log("From Repository" + this._logService.traceId + " " + id)
        return {id : "sampleId"};
    }
}