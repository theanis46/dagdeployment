import { injectable, inject } from "inversify";
import * as _ from 'underscore';
import BaseService from "../../../common/services/implementation/BaseService";
import ISeedService from "../ISeedService";
import SeedDto from "../dto/SeedModel";
import SeedServiceTypes from "../../repositories/types";
import ISeedRepository from "repositories/ISeedRepository";
import { OKResponse } from "../../../common/response/models/OKResponse";
import { BadRequestResponse } from "../../../common/response/models/BadRequestResponse";


@injectable()
export default class SeedService extends BaseService<SeedDto> implements ISeedService {

    @inject(SeedServiceTypes.ISeedRepository)
    private readonly _seedServiceRepo: ISeedRepository;

    protected getDtoClass(): Function {
        return SeedDto;
    }

    async getById(id: string): Promise<any> {
        let dbResponse = await this._seedServiceRepo.getById(id);
        console.log("from service %j" , dbResponse);
        if(dbResponse) {
            return new OKResponse<any>(dbResponse);
        } else {
            return new BadRequestResponse<any>(dbResponse);
        }
    }
}