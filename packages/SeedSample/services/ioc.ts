import { Container } from "inversify";
import ISeedService from "./ISeedService";
import SeedServiceTypes from "./types";
import SeedService from "./implementation/SeedService";

export function configureSeedServices(container: Container) {
    container.bind<ISeedService>(SeedServiceTypes.ISeedService).to(SeedService).inSingletonScope();
    return container;

}