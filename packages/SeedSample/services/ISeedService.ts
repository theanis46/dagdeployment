// import SeedDto from './dto/SeedModel';

export default interface ISeedService {
    getById(id: string): Promise<any>;
}