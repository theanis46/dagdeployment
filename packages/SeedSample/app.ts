import * as express from "express";
import { Container } from 'inversify';
import { InversifyExpressServer } from 'inversify-express-utils';
import { configureCommonServices } from '../common/services/ioc';
import * as bodyParser from 'body-parser';

import { configureSeedServices } from './services/ioc';
import { configureSeedRepositories } from './repositories/ioc';
import cors from '../common/middleware/cors';
import traceMiddleware from '../common/middleware/traceMiddleware';
import './controller/SeedController';

let container = new Container();
configureCommonServices(container, __dirname);
configureSeedRepositories(container);
configureSeedServices(container);

const app: express.Application = express();
app.use(cors());

let server = new InversifyExpressServer(container, app);
server.setConfig((app) => {
app.use(bodyParser.urlencoded({
extended: false
}));
app.use(bodyParser.json());
});

app.use(traceMiddleware());

// let errorHandlerService = container.get<IErrorHandlerService>(commonServiceTypes.IErrorHandlerService);
// server.setErrorConfig(app => {
//     app.use(errorHandlerService.handle.bind(errorHandlerService));
// });

export default server.build();